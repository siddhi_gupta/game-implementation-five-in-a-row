General Instructions
================
--------------------------------

1. Clone this repo directly. 
2. Edit the script using Sublime or any other text editor.
3. Have the input file in the required format.
4. Run the script `fiveInARow.htm` in the browser Mozilla Firefox.

Use of Readme
============
------------------------------

* Use Table of Contents to navigate through the sections
* Review the edit log for recent changes
	* Remember to add you signature and date after you review recent changes!
* When you make a change, create an entry at the top of the Edit Log with the date of the change and your signature.

Links
====
----------

* [Five In A Row](https://en.wikipedia.org/wiki/Gomoku)
* [JavaScript](https://www.codecademy.com/learn/javascript)

Table of Contents
==============
----------------------------------
[TOC]

Five In a Row
===================
----------------------------------------

## Problem Description
This is the implementation of the popular game Five In A Row, also known as Gomoku.  This game is designed to play against the computer. There are 3 levels of difficulty based on the size of the grid.

Edit Log
=======
-----------------
## Complete Creation
> Date: 17/10/13
> 
> **Sections:**
> 
> * General Instructions
> * Use of Readme
> * Links
> * Problem Statement
>
>
> **Read Signature**
> 
> * Siddhi Gupta: 17/10/13